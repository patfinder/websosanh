# -*- coding: utf-8 -*-

# Scrapy settings for CompareSales project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'CompareSales'

SPIDER_MODULES = ['CompareSales.spiders']
NEWSPIDER_MODULE = 'CompareSales.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'CompareSales (+http://www.yourdomain.com)'
