# -*- coding: utf-8 -*-
import scrapy
from scrapy.http.response import Response
from scrapy.http.response.html import HtmlResponse


class VatgiaSpider(scrapy.Spider):
    name = "vatgia"
    allowed_domains = ["vatgia.com"]
    start_urls = (
        # 'http://www.vatgia.com/',
        'http://vatgia.com/home/quicksearch.php?keyword=Samsung+Galaxy+S4+Zoom+SM-C1010',
    )

    def parse(self, response):
        """

        :param response: response content
        :type response: HtmlResponse
        :return:
        """

        stores = response.selector.css('.estore>.simple_tip').extract()
        for store in stores:
            print("Store: %s\n" % store)

        # stores = response.selector.css('.estore>.simple_tip')

        pass
